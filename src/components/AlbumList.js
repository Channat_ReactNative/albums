import React, {Component} from 'react'
import {ScrollView, Text, View} from 'react-native'
import AlbumDetail from './AlbumDetail'


class AlbumList extends Component {

    state = {
        albums: []
    }

    componentDidMount() {
        fetch('https://rallycoding.herokuapp.com/api/music_albums')
            .then(res => res.json()) // convert res to json and pass it to data
            .then(data => {
                this.setState({
                    albums: data
                })
            })
    }


    render() {

        //console.log(this.state.albums);
        const albumsList = this.state.albums.length ? (
            this.state.albums.map(album => {
                return <AlbumDetail key={album.title} album={album}/>
            })
        ) : (
            <View style={{alignItems: 'center'}}>
                <Text >No data</Text>
            </View>
        )

        return (
            <ScrollView>
                {albumsList}
            </ScrollView>
        )
    }
}

export default AlbumList